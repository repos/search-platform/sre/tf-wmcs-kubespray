output "single_server" {
  value = "openstack_compute_instance_v2.single_server.name.${local.dns_domain}"

}

output "multi_servers" {
  value = [for k, v in openstack_compute_instance_v2.kube_workers : "${v.name}.${local.dns_domain}"]

}