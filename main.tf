terraform {
  required_version = ">= 0.14.0"
  required_providers {
    openstack = {
      source = "terraform-provider-openstack/openstack"
    }
  }
}


resource "openstack_compute_instance_v2" "minio" {
  name            = "minio"
  image_name      = var.image_name
  flavor_name     = var.server_flavor
  security_groups = ["default"]
  metadata = {
    owner = var.owner
  }
  user_data = "{file(${path.module}/cloud-init.sh)}"
  network {
    name = "lan-flat-cloudinstances2b"
  }
}

resource "openstack_compute_instance_v2" "kube_workers" {
  count           = var.num_servers
  name            = "kube-worker-${count.index}"
  image_name      = var.image_name
  flavor_name     = local.kube_worker_flavor
  security_groups = ["default"]
  metadata = {
    owner = var.owner
  }
  user_data = "{file(${path.module}/cloud-init.sh)}"
  network {
    name = "lan-flat-cloudinstances2b"
  }

}



resource "local_file" "ansible_inventory" {
  content = templatefile("${path.root}/ansible/hosts.tmpl", {
    minio         = openstack_compute_instance_v2.minio.name
    kube_workers  = openstack_compute_instance_v2.kube_workers.*.name
    domain_suffix = "${local.dns_domain}"
    }
  )
  filename        = "ansible/hosts.ini"
  file_permission = "0644"
  # Sleep a few minutes so DNS can propagate, then run playbook. 
  provisioner "local-exec" {
    command = "sleep 300; ansible-playbook -i ansible/hosts.ini  ansible/init.yml"
  }
}

