provider "openstack" {
}

variable "num_servers" {
  type    = number
  default = 3
}

variable "owner" {
  type    = string
  default = ""
}

variable "server_flavor" {
  type    = string
  default = "g3.cores1.ram2.disk20"
}

variable "image_name" {
  type    = string
  default = "debian-11.0-bullseye"

}

variable "tenant_name" {
  type    = string
  default = ""
}

variable "wmcs_domain" {
  type    = string
  default = "eqiad1.wikimedia.cloud"

}